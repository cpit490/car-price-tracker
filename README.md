[![build status](https://gitlab.com/cpit490/car-price-tracker/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/cpit490/car-price-tracker/-/commits/main)
[![coverage report](https://gitlab.com/cpit490/car-price-tracker/badges/main/coverage.svg)](https://gitlab.com/cpit490/car-price-tracker/-/commits/main)
[![Latest Release](https://gitlab.com/cpit490/car-price-tracker/-/badges/release.svg)](https://gitlab.com/cpit490/car-price-tracker/-/releases)

# Car Price Tracker Example
This is an example that demonstrates the use of a web scraper that collects the prices of used cars.
This project serves as a practical example of a web scraper for collecting data on used car prices from a static page.
The primary purpose of this project is to serve as a demonstrative guide for deploying applications on various
platforms, including Infrastructure as a Service (IaaS) with Virtual Machines (VMs),
Platform as a Service (PaaS), and Function as a Service (FaaS)/serverless architectures.
It is a Java-based application that uses the Jsoup library to scrape data from a static web page. It runs a simple HTTP server to serve requests and
returns the scraped data in JSON format.



## Requirements
- Java 17 or later
- Maven 3.8 or later

## Installation and usage
- Get the latest release from the [releases page](https://gitlab.com/cpit490/car-price-tracker/-/releases)
- run the jar file with the following command:

```shell
java -jar car-price-tracker-1.1.0.jar
```


## Building the project
- Clone the repo
```shell
git clone https://gitlab.com/cpit490/car-price-tracker.git
```
- Set the environment variable `PORT` to the port number you want the server to listen on. 
If the `PORT` environment variable is not set, the server will listen on port 3000 by default.
```shell
export PORT=3000
```
- Run the HTTP server using the following command:

```shell
 mvn compile exec:java -D exec.mainClass="sa.edu.kau.fcit.cpit490.carpricetracker.App"
```

- Send a GET request to the server to get the scraped data in JSON format:

```shell
curl http://localhost:3000/api/car-prices
```
The response will be similar to the following:
```json
[
  {
    "car": "Honda Civic 2019",
    "price": "70,000 SAR",
    "mileage": "45,550 KM"
  },
  {
    "car": "Toyota Corolla 2019",
    "price": "55,000 SAR",
    "mileage": "120,550 KM"
  },
  {
    "car": "Toyota Rav4 2021",
    "price": "85,000 SAR",
    "mileage": "98,000 KM"
  },
  {
    "car": "Mazda CX5 2019",
    "price": "92,000 SAR",
    "mileage": "140,206 KM"
  },
  {
    "car": "Ford Escape 2021",
    "price": "90,000 SAR",
    "mileage": "110,230 KM"
  }
]
```

## License
This project is licensed under the MIT License.