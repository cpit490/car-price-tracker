package sa.edu.kau.fcit.cpit490.carpricetracker;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarPriceScraper {
    private static final Logger logger = LogManager.getLogger(CarPriceScraper.class);

    private List<Map<String, String>> createCarDataList(Elements titles, Elements mileages, Elements prices) {
        List<Map<String, String>> resultList = new ArrayList<>();
        for (int i = 0; i < titles.size(); i++) {
            Map<String, String> map = new HashMap<>();
            map.put("car", titles.get(i).text());
            map.put("mileage", mileages.get(i).text());
            map.put("price", prices.get(i).text());
            logger.info("{}, Mileage: {}, Price: {}",
                    titles.get(i).text(), mileages.get(i).text(), prices.get(i).text());
            resultList.add(map);
        }
        return resultList;
    }

    private Document downloadDocument(String url) throws IOException {
        if (url == null || url.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be null or empty");
        } else if (url.startsWith("file://")) {
            return Jsoup.parse(new URL(url).openStream(), "UTF-8", url);
        } else if (url.startsWith("http://") || url.startsWith("https://")) {
            return Jsoup.connect(url).get();
        } else {
            throw new IllegalArgumentException("URL must start with file://, http://, or https://");
        }
    }

    public String getCarPrices(String url) throws IOException {
        Document doc = downloadDocument(url);
        Elements titles = doc.select("h2.title");
        Elements mileages = doc.select("p.mileage");
        Elements prices = doc.select("p.price");
        if (titles.size() == prices.size() && prices.size() == mileages.size()) {
            List<Map<String, String>> carDataList = createCarDataList(titles, mileages, prices);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(carDataList);
        } else {
            logger.error("Error: titles ({}), mileages ({}), and prices ({}) are not equal",
                    titles.size(), mileages.size(), prices.size());
            return "Error: The number of car names and prices do not match";
        }
    }
}
