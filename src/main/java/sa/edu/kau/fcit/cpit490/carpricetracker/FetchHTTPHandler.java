package sa.edu.kau.fcit.cpit490.carpricetracker;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class FetchHTTPHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange t) throws IOException {
        CarPriceScraper carPriceScraper = new CarPriceScraper();
        final String url = "https://cpit490.gitlab.io/demos/online-store/";
        String response = carPriceScraper.getCarPrices(url);
        t.sendResponseHeaders(200, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
