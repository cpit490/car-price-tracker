package sa.edu.kau.fcit.cpit490.carpricetracker;


import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;

public class App {
    private static HttpServer server;
    private static final Logger logger = LogManager.getLogger(App.class);
    public static void main(String[] args) throws IOException{
        start();
    }

    public static void start() throws IOException {
        // check if port is set in the environment, then use it, otherwise use 3000
        String portValue = System.getenv("PORT");
        int portNumber = portValue != null ? Integer.parseInt(portValue) : 3000;
        server = HttpServer.create(new InetSocketAddress(portNumber), 0);
        server.createContext("/api/car-prices", new FetchHTTPHandler());
        server.setExecutor(null);
        logger.info("Starting server on port {}", portNumber);
        logger.info("You may start sending HTTP get requests to http://localhost:{}/api/car-prices", portNumber);
        server.start();
    }
    public static void stop() {
        logger.info("Stopping server");
        server.stop(0);
    }
}
