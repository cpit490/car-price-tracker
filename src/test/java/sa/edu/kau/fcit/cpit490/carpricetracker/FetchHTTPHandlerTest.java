package sa.edu.kau.fcit.cpit490.carpricetracker;
import com.sun.net.httpserver.HttpExchange;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import org.junit.jupiter.api.Test;

public class FetchHTTPHandlerTest {
    @Test
    public void testHandle() throws IOException {
        HttpExchange httpExchange = Mockito.mock(HttpExchange.class);
        OutputStream outputStream = Mockito.mock(OutputStream.class);
        Mockito.when(httpExchange.getResponseBody()).thenReturn(outputStream);

        CarPriceScraper carPriceScraper = new CarPriceScraper();
        URL url = getClass().getClassLoader().getResource("index.html");
        String fileUrl = "file:///" + url.getPath();
        String expectedResponse = carPriceScraper.getCarPrices(fileUrl);

        FetchHTTPHandler fetchHTTPHandler = new FetchHTTPHandler();
        fetchHTTPHandler.handle(httpExchange);
        Mockito.verify(httpExchange).sendResponseHeaders(200, expectedResponse.length());
        Mockito.verify(outputStream).write(expectedResponse.getBytes());
        Mockito.verify(outputStream).close();
    }
}
