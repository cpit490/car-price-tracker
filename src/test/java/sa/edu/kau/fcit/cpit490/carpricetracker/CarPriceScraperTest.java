package sa.edu.kau.fcit.cpit490.carpricetracker;

import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

public class CarPriceScraperTest {
    @Test
    public void testGetCarPrices() {
        CarPriceScraper carPriceScraper = new CarPriceScraper();
        // get the path to test/resources/index.html
        URL url = getClass().getClassLoader().getResource("index.html");
        assertNotNull(url);
        try {
            String fileUrl = "file:///" + url.getPath();
            ;
            String response = carPriceScraper.getCarPrices(fileUrl);
            assertTrue(response.contains("Toyota"));
            assertTrue(response.matches(".*\\b\\d{1,3}(,\\d{3})*\\s*KM\\b.*"), "Mileage does not contain numbers followed by 'KM'");
            assertTrue(response.matches(".*\\b\\d{1,3}(,\\d{3})*\\s*SAR\\b.*"), "Price does not contain numbers followed by 'SAR'");
        } catch (Exception e) {
            fail("Exception thrown: " + e.getMessage());
        }
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenUrlIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            CarPriceScraper carPriceScraper = new CarPriceScraper();
            carPriceScraper.getCarPrices(null);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            CarPriceScraper carPriceScraper = new CarPriceScraper();
            carPriceScraper.getCarPrices("ftp://example.com");
        });
    }
}
