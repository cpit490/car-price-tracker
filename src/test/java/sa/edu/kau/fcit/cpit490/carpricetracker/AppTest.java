package sa.edu.kau.fcit.cpit490.carpricetracker;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class AppTest {
    @Test
    public void testApp() {
        assertDoesNotThrow(() -> {
            assertTimeout(Duration.ofMinutes(1), () -> {
                App.start();
                // Wait for 5 seconds to allow the server to start
                Thread.sleep(5000);
                // Shut down the server
                App.stop();
            });
        });
    }
}
